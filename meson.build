# This file is distributed under the GNU All-Permissive License.
# Copying and distribution of this file, with or without modification,
# are permitted in any medium without royalty provided the copyright
# notice and this notice are preserved.
# This file is offered as-is, without any warranty.

project(
    'amalthea',
    'd',
    version: run_command('cat', 'source/amalthea/version').stdout().strip()
)

project_soversion = 1
project_dependencies = [dependency('glib-2.0'), dependency('gio-2.0')]
project_description = 'Small general Linux-specific library'

project_name = meson.project_name()
dc = meson.get_compiler('d').cmd_array()[0]

r = run_command('find', 'source/' + project_name, '-name', '*.d')
source_files = r.stdout().split()

shlib = shared_library(
    project_name + '-' + dc,
    source_files,
    include_directories: include_directories(['source']),
    dependencies: project_dependencies,
    install: true,
    d_args: ['-J..'],
    version: meson.project_version(),
    soversion: project_soversion
)
stlib = static_library(
    project_name + '-' + dc,
    source_files,
    include_directories: include_directories(['source']),
    dependencies: project_dependencies,
    install: true,
    d_args: ['-J..']
)

pkgc = import('pkgconfig')
pkgc.generate(
    name: project_name + '-' + dc,
    libraries: shlib,
    subdirs: dc + '/' + project_name,
    version: meson.project_version(),
    description: project_description + ', version for ' + dc
)
install_subdir('source/' + project_name, install_dir: 'include/' + dc)

