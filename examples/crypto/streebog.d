#!/usr/bin/env -S rdmd --compiler=ldc2

/* Copying and distribution of this file, with or without modification,
 * are permitted in any medium without royalty provided the copyright
 * notice and this notice are preserved.  This file is offered as-is,
 * without any warranty.
 */

import amalthea.crypto;
import std.stdio;

int main(string[] args) {
    if (args.length != 2) {
        stderr.writeln("Wrong usage.");
        return 1;
    }
    string pathToFile = args[1];
    getFileStreebog256Sum(pathToFile).writeln;
    return 0;
}
