#!/usr/bin/env -S rdmd --compiler=ldc2

/* Copying and distribution of this file, with or without modification,
 * are permitted in any medium without royalty provided the copyright
 * notice and this notice are preserved.
 * This file is offered as-is, without any warranty.
 */

import std.stdio;
import amalthea.sys : createAnonymousMapping, destroyMapping;

void main() {
    size_t size = 64;
    // allocate 128 bytes and create C-style array
    ulong* arr = cast(ulong*) createAnonymousMapping(size);  // 8 elements
    // fill array
    for (size_t i = 0; i < size / ulong.sizeof; i++) {
        arr[i] = i;
    }
    // use array
    for (size_t i = 0; i < size / ulong.sizeof; i++) {
        writeln(arr[i]);
    }
    // free allocated memory
    destroyMapping(arr, size);
}
