#!/usr/bin/env -S rdmd --compiler=ldc2

/* Copying and distribution of this file, with or without modification,
 * are permitted in any medium without royalty provided the copyright
 * notice and this notice are preserved.
 * This file is offered as-is, without any warranty.
 */
import std.stdio : writeln;
import amalthea.sys : getMountPoints;


void main(string[] args) {
    if (args.length != 2) {
        return;
    }
    string blockDevice = args[1];
    foreach(mp; getMountPoints(blockDevice)) {
        writeln(mp);
    }
}
