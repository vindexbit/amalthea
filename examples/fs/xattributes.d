#!/usr/bin/env -S rdmd --compiler=ldc2

/* Copying and distribution of this file, with or without modification,
 * are permitted in any medium without royalty provided the copyright
 * notice and this notice are preserved.  This file is offered as-is,
 * without any warranty.
 */
import std.stdio;
import std.file;
import std.ascii : letters;
import std.conv : to;
import std.path;
import std.random : randomSample;
import std.utf : byCodeUnit;

import amalthea.fs : xattr;


string genTempFileName() {
    auto id = letters.byCodeUnit.randomSample(16).to!string;
    return std.path.buildPath(tempDir, "tmp_file_" ~ id);
}


void main(string[] args) {
    string tempFile = genTempFileName();
    std.file.write(tempFile, "");
    scope(exit) std.file.remove(tempFile);

    writeln("Attributes: ", xattr.getAttrs(tempFile));  // empty

    // custom keys must be start with "user."
    xattr.set(tempFile, "user.space_address.planet_name", "Earth");
    xattr.set(tempFile, "user.space_address.related_star", "Sun");

    writeln("Attributes: ", xattr.getAttrs(tempFile));

    writeln(xattr.read(tempFile, "user.space_address.planet_name"));  // "Earth"
    writeln(xattr.getAttrsAndValues(tempFile));  // print all

    xattr.remove(tempFile, "user.space_address.related_star");
    writeln(xattr.getAttrsAndValues(tempFile));  // without "Sun"
}
