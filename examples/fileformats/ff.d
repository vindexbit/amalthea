#!/usr/bin/env -S rdmd --compiler=ldc2
/* Copying and distribution of this file, with or without modification,
 * are permitted in any medium without royalty provided the copyright
 * notice and this notice are preserved.  This file is offered as-is,
 * without any warranty.
 */

import std.stdio : writefln;

import amalthea.fs;
import amalthea.fileformats;

int main(string[] args) {
    string dir = args[1];
    if (args.length > 2) {
        addNewPathToFindFileFormats(args[2]);
    }
    FileEntry[] files = getRegularFiles(
        dir, recursively: Yes.recursively, saveAbsolutePath: No.absPath
    );
    foreach(f; files) {
        writefln("File:        %s", f.path);
        FileFormat ff = getFileFormat(f.path);
        writefln("Format:      %s", ff.format);
        writefln("Description: %s", ff.description);
        writeln("================================");
    }
    return 0;
}
